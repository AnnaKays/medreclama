import IMask from 'imask';

const inputPhone = document.getElementById('tel');
const inputName = document.getElementById('name');
const inputs = document.querySelectorAll('.form-input__field');
const formBtn = document.querySelector('.sales-form__btn');

const mask = IMask(inputPhone, {
  mask: '+{7} (000) 000-00-00',
  country: 'Russia',
});

inputPhone.addEventListener('focus', () => {
  mask.updateOptions({ lazy: false });
});

inputPhone.addEventListener('blur', () => {
  mask.updateOptions({ lazy: true });
});

const onBtnClick = () => {
  if (inputName.value !== '') {
    inputName.classList.add('valid');
  } else {
    inputName.classList.add('invalid');
  }
  if (mask.unmaskedValue.length !== 11) {
    inputPhone.classList.add('invalid');
  } else {
    inputPhone.classList.add('valid');
  }
};

const addBtnValidation = () => {
  formBtn.addEventListener('click', onBtnClick);
};

inputs.forEach((input) => {
  input.addEventListener('blur', () => {
    input.classList.remove('invalid');
    input.classList.remove('valid');
  });
});

export default addBtnValidation;
