import Swiper, { Navigation } from 'swiper';

const specialistSlider = () => new Swiper('.specialist__slider', {
  modules: [Navigation],
  direction: 'horizontal',
  loop: true,
  watchSlidesProgress: true,
  speed: 500,
  slideToClickedSlide: true,
  grabCursor: true,
  spaceBetween: 40,

  breakpoints: {
    375: {
      slidesPerView: 1,
    },
    480: {
      slidesPerView: 2,
    },
    800: {
      slidesPerView: 4,
    },
  },

  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});

export default specialistSlider;
