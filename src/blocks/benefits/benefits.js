import Sortable from 'sortablejs';

const dragEl = document.querySelector('.benefits');
const initDrag = () => {
  Sortable.create(dragEl, {
    animation: 200,
    easing: 'cubic-bezier(.17,.67,.83,.67)',
    handle: 'h3',
  });
};

export default initDrag;
