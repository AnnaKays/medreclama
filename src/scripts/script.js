import addBtnValidation from '../blocks/form-input/form-input';
import specialistSlider from '../blocks/specialist/specialist';
import initDrag from '../blocks/benefits/benefits';

initDrag();
specialistSlider();
addBtnValidation();
